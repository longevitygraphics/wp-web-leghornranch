<?php

function lg_reviews_cpt() {
	register_post_type(
		'lg-reviews',
		array(
			'label'             => 'Reviews',
			'public'            => true,
			'show_in_rest'      => true,
			'show_in_menu'      => false,
			'show_ui'           => true,
			'show_in_admin_bar' => true,
			'supports'          => array( 'editor', 'title', 'thumbnail' ),
		)
	);
}

function lg_reviews_menu() {
	add_submenu_page(
		$GLOBALS['lg_main_menu'],
		'Reviews',
		'Reviews',
		'manage_options',
		'edit.php?post_type=lg-reviews'
	);
}

add_action( 'init', 'lg_reviews_cpt', 10 );
add_action( 'admin_init', 'lg_reviews_menu', 10 );
