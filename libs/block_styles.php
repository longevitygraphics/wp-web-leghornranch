<?php

function add_list_styles() {

	register_block_style(
		'core/list',
		array(
			'name'  => 'horseshoe-list',
			'label' => __( 'Horseshoe Style List', 'lg-blocks' ),
		)
	);
}

add_action( 'init', 'add_list_styles' );
