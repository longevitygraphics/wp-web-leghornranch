const defaultConfig = require('@wordpress/scripts/config/webpack.config.js');
const path = require( 'path' );
const postcssPresetEnv = require( 'postcss-preset-env' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

const production = process.env.NODE_ENV === '';

module.exports = {
	...defaultConfig,
	entry: {
        main: path.resolve( process.cwd(), 'src', 'js', 'script.js'),
        editorjs: path.resolve( process.cwd(), 'src', 'js', 'editor.js'),
		frontend: path.resolve( process.cwd(), 'src', 'sass', 'style.scss' ),
		editor: path.resolve( process.cwd(), 'src', 'sass', 'styles.editor.scss' ),
	},
	plugins: [
		...defaultConfig.plugins,
	 	new MiniCssExtractPlugin( {
			filename: '[name].css',
	 	} ),
	 ],
};