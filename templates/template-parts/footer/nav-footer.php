<?php
	wp_nav_menu( array(
		'theme_location' => 'bottom-nav',
		'menu_id'        => 'bottom-nav',
		'depth'          => 1,
		'container'      => 'div'
	) );
?>