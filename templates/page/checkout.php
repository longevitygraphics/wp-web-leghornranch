<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container py-5">
			<h1 class="mt-4 mb-4 has-brown-color has-text-color">Checkout</h1>
				<?php echo do_shortcode('[woocommerce_checkout]'); ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?>