<?php
/**
 * The header for our theme
 */


if (function_exists("get_field") ) {

$booking_link = get_field( 'booking_link', 'options' );
$camping_link = get_field( 'camping_link', 'options' );
$waiver_link = get_field( 'waiver_link', 'options' );

}

?>

<?php do_action( 'document_start' ); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action( 'html_class' ); ?>>
<head>
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <?php do_action( 'wp_header' ); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action( 'wp_body_start' ); ?>

	<div id="page" class="site">

	<header id="site-header">

	<?php do_action( 'wp_utility_bar' ); ?>

	<div class="header-main">

	<div class="upper-header">
		<a class="navbar-brand" href="<?php echo get_site_url(); ?>">
			<div class="logo-group">
				<span class="logo-top">Leghorn Ranch</span><span class="logo-bottom">Vancouver Horseback Riding</span>
			</div>	
		</a>
	</div>
	<div class="lower-header">
		<?php get_template_part( '/templates/template-parts/header/main-nav' ); ?>
		<div class="book-now d-none d-md-flex">
			<a href="<?php echo esc_attr( $booking_link['url'] ); ?>"><p>Book a Trail Ride Now</p></a>
		</div>
		<div class="icon-group">
			<div class="icon-group__register">
				<a href="<?php echo esc_attr( $camping_link['url'] ); ?>" class="icon-register"></a>
				<a href="<?php echo esc_attr( $camping_link['url'] ); ?>" class="d-none d-md-block"><span >Register for Camp</span></a>
			</div>
			<div class="icon-group__sign">
				<a href="<?php echo esc_attr( $waiver_link['url'] ); ?>" class="icon-sign"></a>
				<a href="<?php echo esc_attr( $waiver_link['url'] ); ?>" class="d-none d-md-block"><span >Sign Waiver</span></a>
			</div>
		</div>
		
		</div>

		<div class="mobile-book-now d-flex d-md-none">
			<div class="mobile-book-now__icon">
				<a href="<?php echo esc_attr( $booking_link['url'] ); ?>" class="mobile-book-now__text"> <span>Book A Trail Ride Now</span></a>
			</div>
		</div>
	</div>
	</header><!-- #masthead -->

	<div id="site-content" role="main">
	<?php do_action( 'wp_content_top' ); ?>
