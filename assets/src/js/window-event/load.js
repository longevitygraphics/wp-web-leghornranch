// Windows Load Handler

(function ($) {
    $(window).on("load", function () {
        console.log("should be something happening");
        if ($(".am-filter-icon")) {
            $(".am-filter-icon").addClass("pulse");

            $(".am-filter-icon").on("click", () => {
                $(".am-filter-icon").removeClass("pulse");
            });
        }
    });
})(jQuery);

let cls = 0;
new PerformanceObserver((entryList) => {
    for (const entry of entryList.getEntries()) {
        if (!entry.hadRecentInput) {
            cls += entry.value;
            console.log("Current CLS value:", cls, entry);
            //debugger;
            if (cls >= 0.1) {
                console.log("CLS FAILING");
            }
        }
    }
}).observe({ type: "layout-shift", buffered: true });
